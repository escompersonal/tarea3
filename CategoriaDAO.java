import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class CategoriaDAO  {

	public boolean create(Categoria dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(InterfaceCRUD.SQL_INSERT);
            ps.setInt(1, dto.getId());
            ps.setString(2, dto.getNombre());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public boolean delete(Categoria dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(InterfaceCRUD.SQL_DELETE);
            ps.setInt(1, dto.getId());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);            
        }
    }

    public boolean update(Categoria dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(InterfaceCRUD.SQL_UPDATE);
            ps.setString(1, dto.getNombre());
            ps.setInt(2,dto.getId());            
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);          
        }
    }

    public Categoria load(Categoria dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(InterfaceCRUD.SQL_SELECT);
            ps.setInt(1, dto.getId());
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return (Categoria) results.get(0);
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }

    public List loadAll(Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(InterfaceCRUD.SQL_SELECT_ALL);
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return results;
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }


    private List getResults(ResultSet rs) throws SQLException {
        List results = new ArrayList();
        while (rs.next()) {
            Categoria dto = new Categoria();
            dto.setId(rs.getInt("id_categoria"));
            dto.setNombre(rs.getString("nomb"));
            results.add(dto);
        }
        return results;
    }

    private void cerrar(PreparedStatement ps) throws SQLException {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }
    private void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }
    private void cerrar(Connection cnn) {
        if (cnn != null) {
            try {
                cnn.close();
            } catch (SQLException e) {
            }
        }
    }
	
	
}