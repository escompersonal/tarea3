import java.util.List;
import java.sql.SQLException;

public interface InterfaceCRUD{

	public static final String SQL_INSERT =
            "INSERT INTO Categoria ("
            + "id_categoria, nomb"
            + ") VALUES (?, ?)";
            
    public static final String SQL_SELECT =
            "SELECT id_categoria, nomb "
            + "  FROM Categoria where id_categoria= ?";

    public static final String SQL_SELECT_ALL =
            "SELECT  nomb, id_categoria"
            +" FROM Categoria";

    public static final String SQL_UPDATE =
            "UPDATE Categoria SET "
            + "nomb = ?"
            + " WHERE "
            + "id_categoria = ? ";
    /* SQL to delete data */
    public static final String SQL_DELETE =
            "DELETE FROM Categoria WHERE "
            + "id_categoria = ?";

	public boolean alta(Categoria c)throws SQLException;
    public boolean baja(Categoria c)throws SQLException;
    public boolean editar(Categoria c)throws SQLException;
    public Categoria buscar(Categoria id)throws SQLException;
    public List<Categoria> listar()throws SQLException;	
}